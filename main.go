package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func welcome(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome Home")
}

func getPeople(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Get People")
}

func getPerson(w http.ResponseWriter, r *http.Request) {

}

func createPerson(w http.ResponseWriter, r *http.Request) {

}

func deletePerson(w http.ResponseWriter, r *http.Request) {

}

func upload(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Upload files\n")

	file, handler, err := r.FormFile("file")
	if err != nil {
		panic(err) //dont do this
	}
	defer file.Close()

	// copy example
	fileLocation := "./uploads/" + handler.Filename
	f, err := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(err) //please dont
	}
	defer f.Close()
	io.Copy(f, file)
}

// our main function
func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", welcome).Methods("GET")
	router.HandleFunc("/upload", upload).Methods("POST")
	router.HandleFunc("/people", getPeople).Methods("GET")
	router.HandleFunc("/people/{id}", getPerson).Methods("GET")
	router.HandleFunc("/people/{id}", createPerson).Methods("POST")
	router.HandleFunc("/people/{id}", deletePerson).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8123", router))
}
